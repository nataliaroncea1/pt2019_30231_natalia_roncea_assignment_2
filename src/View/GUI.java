package View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.Timer;

import Controller.Data;
import Controller.Logger;
import Model.Client;
import Model.Queue;

/**
 * Interfata grafica cu utilizatorul
 * 
 *
 *
 */
public class GUI extends Thread {
	/**
	 * atributele clasei
	 */
	ArrayList<Client> clienti = new ArrayList<Client>();
	ArrayList<Queue> queues = new ArrayList<Queue>();
	int durataMedieAsteptare = 0;
	int durataMedieServiciu = 0;
	int it = 1;
	int it2 = 1;
	String old = "";

	/**
	 * constructorul
	 */
	public GUI() {
		/**
		 * frame-ul principal
		 */
		JFrame f = new JFrame("");
		f.getContentPane().setBackground(new Color(200, 255, 255));

		JLabel t = new JLabel("Aplicatie de management al supermarketului");
		t.setBounds(60, 15, 600, 40);
		t.setForeground(new Color(150, 10, 50));
		t.setFont(new Font("Monotype Corsiva", Font.BOLD, 29));
		f.getContentPane().add(t);

		JLabel t2 = new JLabel("Date de intrare:");
		t2.setBounds(60, 150, 500, 40);
		t2.setForeground(new Color(150, 10, 50));
		t2.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		f.getContentPane().add(t2);

		JLabel tC = new JLabel("interval min asteptare:");
		tC.setBounds(50, 190, 200, 40);
		tC.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(tC);

		JTextField t2C = new JTextField();
		t2C.setBounds(50, 220, 140, 30);
		t2C.setForeground(new Color(0, 0, 50));
		f.getContentPane().add(t2C);

		JLabel t3C = new JLabel("interval max asteptare:");
		t3C.setBounds(200, 190, 200, 40);
		t3C.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(t3C);

		JTextField t4C = new JTextField();
		t4C.setBounds(200, 220, 140, 30);
		t4C.setForeground(new Color(0, 0, 50));
		f.getContentPane().add(t4C);

		JLabel d = new JLabel("Durata minima serviciu:");
		d.setBounds(50, 260, 200, 40);
		d.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(d);

		JTextField dd = new JTextField();
		dd.setBounds(50, 290, 140, 30);
		dd.setForeground(new Color(0, 0, 50));
		f.getContentPane().add(dd);

		JLabel d2 = new JLabel("Durata maxima serviciu:");
		d2.setBounds(200, 260, 200, 40);
		d2.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(d2);

		JTextField dd2 = new JTextField();
		dd2.setBounds(200, 290, 140, 30);
		dd2.setForeground(new Color(0, 0, 50));
		f.getContentPane().add(dd2);

		JLabel sim = new JLabel("interval de simulare:");
		sim.setBounds(400, 190, 200, 40);
		sim.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(sim);

		JTextField tsim = new JTextField();
		tsim.setBounds(400, 220, 140, 30);
		tsim.setForeground(new Color(0, 0, 50));
		f.getContentPane().add(tsim);

		JLabel cl = new JLabel("Clientii din supermarket:");
		cl.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		cl.setBounds(20, 90, 200, 30);
		cl.setForeground(new Color(0, 0, 0));
		f.getContentPane().add(cl);

		JLabel cli = new JLabel();
		cli.setBounds(300, 90, 400, 30);
		cli.setBackground(new Color(200, 255, 255));
		cli.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		cli.setText("");
		f.getContentPane().add(cli);

		JLabel co = new JLabel("Coada1 :");
		co.setBounds(100, 370, 100, 40);
		co.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(co);

		JLabel coaf = new JLabel();
		coaf.setBounds(250, 370, 200, 50);
		coaf.setBackground(new Color(200, 255, 255));
		coaf.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		coaf.setText("");
		f.getContentPane().add(coaf);

		JLabel co2 = new JLabel("Coada2 :");
		co2.setBounds(100, 420, 100, 40);
		co2.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(co2);

		JLabel coaf2 = new JLabel();
		coaf2.setBounds(250, 420, 200, 50);
		coaf2.setBackground(new Color(200, 255, 255));
		coaf2.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		coaf2.setText("");
		f.getContentPane().add(coaf2);

		JLabel co3 = new JLabel("Coada3 :");
		co3.setBounds(100, 470, 100, 40);
		co3.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(co3);

		JLabel coaf3 = new JLabel();
		coaf3.setBounds(250, 470, 200, 50);
		coaf3.setBackground(new Color(200, 255, 255));
		coaf3.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		coaf3.setText("");
		f.getContentPane().add(coaf3);

		JLabel dma = new JLabel("Durata medie de asteptare:");
		dma.setBounds(93, 520, 200, 40);
		dma.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(dma);

		JLabel dmaa = new JLabel();
		dmaa.setBounds(250, 520, 200, 50);
		dmaa.setBackground(new Color(200, 255, 255));
		dmaa.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		f.getContentPane().add(dmaa);

		JButton b = new JButton("DMA");
		b.setBounds(400, 520, 100, 30);
		f.getContentPane().add(b);

		JButton bs = new JButton("DMS");
		bs.setBounds(400, 550, 100, 30);
		f.getContentPane().add(bs);

		JButton bl = new JButton("Logger");
		bl.setBounds(500, 390, 100, 30);
		f.getContentPane().add(bl);

		JLabel dms = new JLabel("Durata medie de serviciu:");
		dms.setBounds(93, 550, 200, 40);
		dms.setForeground(new Color(150, 10, 50));
		f.getContentPane().add(dms);

		JLabel dmss = new JLabel();
		dmss.setBounds(250, 550, 200, 50);
		dmss.setBackground(new Color(200, 255, 255));
		dmss.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		f.getContentPane().add(dmss);

		Queue q1 = new Queue();
		Queue q2 = new Queue();
		Queue q3 = new Queue();
		/**
		 * timerul pentru adaugare
		 */
		Timer timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				String s1 = t2C.getText();
				String s2 = t4C.getText();
				int min = Integer.parseInt(s1);
				int max = Integer.parseInt(s2);

				Random random = new Random();
				int clientGenerat = showRandomInteger(min, max, random);
				Client c = new Client();
				c.setIdClient(clientGenerat);

				String af = "\n Clientul intra in magazin!";
				String af2 = "clientul adaugat are id-ul " + c.getIdClient();
				old = old + af + "\n\n" + af2 + "\n\n";
				System.out.println("\n Clientul intra in magazin!");
				System.out.print("clientul adaugat are id-ul " + c.getIdClient());
				Data d = new Data();
				d.af_data();

				/**
				 * durata minima de serviciu pentru fiecare client
				 */
				int min2 = Integer.parseInt(dd.getText());
				int max2 = Integer.parseInt(dd2.getText());

				Random random2 = new Random();
				int durata = showRandomInteger(min2, max2, random2);
				c.setTimp_asteptare(durata);
				System.out.println(" ---si durata de asteptare " + durata);

				durataMedieAsteptare = durataMedieAsteptare + clientGenerat;
				durataMedieServiciu = durataMedieServiciu + durata;

				if (it == 15) {
					((Timer) e.getSource()).stop();
				}
				if (it % 3 == 1) {

					String strr = coaf.getText();
					coaf.setText(strr + "*");
					q1.addCoada(c);

				}
				if (it % 3 == 2) {

					String strr = coaf2.getText();
					coaf2.setText(strr + "*");
					q2.addCoada(c);
				}

				if (it % 3 == 0) {

					String strr = coaf3.getText();
					coaf3.setText(strr + "*");
					q3.addCoada(c);
				}

				String str = cli.getText();
				cli.setText(str + "0");
				it++;
				clienti.add(c);

			}

		});
		/**
		 * timer-ul pentru stergere
		 */
		Timer timer2 = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int minq = 1;
				int maxq = 3;
				Random rand = new Random();
				int c = showRandomInteger(minq, maxq, rand);
				if (it2 == 16) {
					((Timer) e.getSource()).stop();
				}
				if (c == 1) {
					String s1 = coaf.getText();
					if (s1.length() != 0) {
						String n1cpy = s1.substring(1, s1.length());
						coaf.setText(n1cpy);
					}
					/**
					 * pt contorizarea clientilor totali
					 */
					String str = cli.getText();
					if (str.length() != 0) {
						String strcp = str.substring(1, str.length());
						cli.setText(strcp);

					}
				}
				if (c == 2) {
					String s1 = coaf2.getText();
					if (s1.length() != 0) {
						String n1cpy = s1.substring(1, s1.length());
						coaf2.setText(n1cpy);
					}
					/**
					 * pt contorizarea clientilor totali
					 */
					String str = cli.getText();
					if (str.length() != 0) {
						String strcp = str.substring(1, str.length());
						cli.setText(strcp);

					}
				}
				if (c == 3) {
					String s1 = coaf3.getText();
					if (s1.length() != 0) {
						String n1cpy = s1.substring(1, s1.length());
						coaf3.setText(n1cpy);
					}
					/**
					 * pt contorizarea clientilor totali
					 */
					String str = cli.getText();
					if (str.length() != 0) {
						String strcp = str.substring(1, str.length());
						cli.setText(strcp);

					}
				}
				it++;
			}
		});

		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				durataMedieAsteptare = durataMedieAsteptare / 15;
				System.out.println("DMA: " + durataMedieAsteptare);
				dmaa.setText("     " + durataMedieAsteptare);

			}

		});
		bs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				durataMedieServiciu = durataMedieServiciu / 15;
				System.out.println("DMS: " + durataMedieServiciu);
				dmss.setText("     " + durataMedieServiciu);

			}

		});

		bl.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Logger log = new Logger();
				log.writelog(old);

			}

		});

		JButton b2 = new JButton("start");
		b2.setBounds(500, 290, 100, 30);
		f.getContentPane().add(b2);

		JButton b3 = new JButton("stop");
		b3.setBounds(500, 340, 100, 30);
		f.getContentPane().add(b3);

		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timer.start();

			}
		});

		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				timer.stop();
				timer2.start();

			}
		});

		// f.setBackground(new Color(200,200,19));
		f.setBounds(100, 50, 800, 650);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.getContentPane().setLayout(null);
		f.setVisible(true);

	}

	/**
	 * metoda care genereaza random numere
	 * 
	 * @param aStart
	 * @param aEnd
	 * @param aRandom
	 * @return
	 */
	private static int showRandomInteger(int aStart, int aEnd, Random aRandom) {
		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		// get the range, casting to long to avoid overflow problems
		long range = (long) aEnd - (long) aStart + 1;
		// compute a fraction of the range, 0 <= frac < range
		long fraction = (long) (range * aRandom.nextDouble());
		int randomNumber = (int) (fraction + aStart);
		return randomNumber;

	}

}
