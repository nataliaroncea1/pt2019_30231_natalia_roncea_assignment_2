package Controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JOptionPane;

/**
 * clasa folosita pentru afisarea intr-un fisier text
 * 
 *
 *
 */
public class Logger {
	/**
	 * constructor gol
	 */
	public Logger() {
	}

	public void writelog(String str) {

		try {
			FileOutputStream fout = new FileOutputStream("logger.txt");
			PrintStream ps = new PrintStream(fout);
			ps.println("\n\n\n");
			ps.println("\n\t\t"+str);
			ps.println("\n\n\n");

			ps.close();
			try {
				fout.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			System.out.println(" logger error");
			e.printStackTrace();
		}

	}
}