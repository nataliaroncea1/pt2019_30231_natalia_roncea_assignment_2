package Model;

import java.awt.Component;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.JOptionPane;

/**
 * Clasa coada ce contine clientii
 * 
 * @author Ioana Niculai
 *
 */
public class Queue {
	public int id = 0;
	public ArrayList<Client> list;
	public int nr_clienti = 0;

	/**
	 * constructorul clasei
	 */
	public Queue() {
		id++;
		list = new ArrayList<Client>();

	}

	/**
	 * returneaza true sau false daca o coada este goala sau nu
	 * 
	 * @return
	 */
	public boolean isEmpty()
	// returneaza true daca coada e goala
	{
		return (list.size() == 0);
	}

	/**
	 * afiseaza un mesaj daca o coada este goala
	 * 
	 * @return
	 */
	public String listaGoala() {
		if (list.size() == 0)
			return "lista e goala!";

		else
			return "lista nu e goala";
	}

	/**
	 * metoda ce adauga un client intr-o coada
	 * 
	 * @param c
	 */
	public void addCoada(Client c) {

		list.add(c);
		this.nr_clienti++;
	}

	/**
	 * metoda ce sterge clientul din coada
	 * 
	 * @param c
	 */
	public void removeCoada(Client c) {
		list.remove(c);
		nr_clienti--;
	}

	/**
	 * gettere si settere
	 * 
	 * @return
	 */
	public int calcNrClienti() {
		return nr_clienti;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Object peek() {
		return list.get(1);
	}
}