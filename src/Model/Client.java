package Model;

import java.text.SimpleDateFormat;

import javax.swing.JLabel;

/**
 * Clasa ce contine informatiile despre un client
 * 
 * @author Ioana Niculai
 *
 */
public class Client implements Comparable<Client> {
	public int timp_asteptare;
	public int nr_coada;
	public int idClient;

	/**
	 * am definit mai multi constructori
	 * 
	 */
	public Client(int timp_asteptare, int nr_coada, int idClient) {
		this.timp_asteptare = timp_asteptare;
		this.nr_coada = nr_coada;
		this.idClient = idClient;
	}

	public Client(int timp_asteptare, int nr_coada) {
		this.timp_asteptare = timp_asteptare;
		this.nr_coada = nr_coada;
	}

	/**
	 * constructor gol
	 */

	public Client() {
	}

	/**
	 * gettere si settere
	 * 
	 * @return
	 */

	public int getTimp_asteptare() {
		return timp_asteptare;
	}

	public void setTimp_asteptare(int timp_asteptare) {
		this.timp_asteptare = timp_asteptare;
	}

	public int getNr_coada() {
		return nr_coada;
	}

	public void setNr_coada(int nr_coada) {
		this.nr_coada = nr_coada;
	}

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}

	@Override
	public int compareTo(Client arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}
